#ifndef __PROXY_OUT_H
#define __PROXY_OUT_H

#if defined(__cplusplus)
extern "C" { 
#endif

void poly_2_proxy();
double funcA_proxy();

#if defined(__cplusplus)
}
#endif

#endif

