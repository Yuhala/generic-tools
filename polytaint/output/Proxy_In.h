#ifndef __PROXY_IN_H
#define __PROXY_IN_H

#if defined(__cplusplus)
extern "C" { 
#endif

void sayHello_proxy();

#if defined(__cplusplus)
}
#endif

#endif

