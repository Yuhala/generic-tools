function untrusted_func_0() {

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function untrusted_func_1() {

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function untrusted_func_2() {

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function untrusted_func_3() {

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function untrusted_func_4() {

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function untrusted_func_5() {

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function untrusted_func_6() {

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function untrusted_func_7() {

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function untrusted_func_8() {

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function untrusted_func_9() {

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function untrusted_func_10() {

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function untrusted_func_11() {

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function untrusted_func_12() {

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function untrusted_func_13() {

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function trusted_func_0() {
	var my_int = Polyglot.eval("secL", "sInt(123)");

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function trusted_func_1() {
	var my_int = Polyglot.eval("secL", "sInt(123)");

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function trusted_func_2() {
	var my_int = Polyglot.eval("secL", "sInt(123)");

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function trusted_func_3() {
	var my_int = Polyglot.eval("secL", "sInt(123)");

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function trusted_func_4() {
	var my_int = Polyglot.eval("secL", "sInt(123)");

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
function trusted_func_5() {
	var my_int = Polyglot.eval("secL", "sInt(123)");

    var size = 100;    
    var arr = new Array(size);  
    arr.fill(Math.random());
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = Math.floor(Math.random() * size);
    }
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        let swapped = false;
        for (let j = 0; j < n - i - 1; j++) {

            if (arr[j] > arr[j + 1]) {
                let tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }



}
untrusted_func_0();
untrusted_func_1();
untrusted_func_2();
untrusted_func_3();
untrusted_func_4();
untrusted_func_5();
untrusted_func_6();
untrusted_func_7();
untrusted_func_8();
untrusted_func_9();
untrusted_func_10();
untrusted_func_11();
untrusted_func_12();
untrusted_func_13();
trusted_func_0();
trusted_func_1();
trusted_func_2();
trusted_func_3();
trusted_func_4();
trusted_func_5();
